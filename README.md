# GlobalE

## Description

This project is designed to perform three main tasks:
1. **Prime Number Checker REST API**: Create a REST API that checks whether a given number is prime.
2. **Rate Limiting**: Implement rate limiting to control the number of requests a user can make to the API within a specified time window.
3. **User-Friendly Web Interface**: Develop a simple HTML and JavaScript page that enhances the user experience by interacting with the API.

## Prerequisites

Before getting started, make sure you have the following prerequisites installed:
- [Python](https://www.python.org/downloads/) (for creating the REST API)
- Basic knowledge of HTML and JavaScript for the web interface.

## Installation and Setup

1. **API Setup**:
   - Clone this repository to your local machine.
   - Navigate to the API directory and run `python manage.py runserver` to start the API server.
   - The API will be accessible at `http://localhost:8000` by default.

2. **Rate Limiting**:
   - Configure your caching library for rate limiting in the API code.
   - Adjust the rate limit and time window according to your requirements.

3. **Web Interface**:
   - Create an HTML page (e.g., `index.html`) and a JavaScript file (e.g., `script.js`) for the user interface.
   - Ensure the JavaScript code in your HTML file interacts with the API endpoints for prime number checking.
   - Serve your HTML and JavaScript files using a local server or any hosting solution.


## Usage

1. **Prime Number Checker API**:
   - Make GET requests to `http://localhost:8000/api/` to check if a number is prime.
   - Replace `<number>` with the integer you want to check for primality.
   - The API will respond with a JSON object indicating whether the number is prime.

2. **Rate Limiting**:
   - Test the rate limiting feature by making multiple requests within the specified time window.
   - Observe how the API responds when the rate limit is exceeded.

3. **User-Friendly Web Interface**:
   - Open the HTML page you created (`index.html`) in a web browser.
   - Enter a number in the input field and click a button to check if it's prime.
   - Ensure that the web page displays the result returned by the API.


## clarification

the GlobalE and prime folder are the backend side of the project, the GlobalE is the project and the 
prime is the main app.
