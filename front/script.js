function checkPrime() {
    // Get the form element
    const form = document.getElementById('primeForm');
    // Create an object to store the form data
    const formData = {
        id: form.elements.id.value,
        num: parseFloat(form.elements.num.value)
    };

    // Create an XMLHttpRequest object
    const xhr = new XMLHttpRequest();

    // Define the URL of the 'IsPrimeNumber' endpoint
    const url = 'http://127.0.0.1:8000/api/';

    // Configure the request as a POST request
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');

    // Set up a callback function to handle the response
    xhr.onload = function () {
        if (xhr.status === 200) {
            const response = JSON.parse(xhr.responseText);
//            console.log(response);
            const resultDiv = document.getElementById('result');
            console.log(response.IsPrime)
            if (response.IsPrime) {
                resultDiv.innerHTML = `${formData.num} is a prime number.`;
            }
            else if (response.IsPrime === false) {
                resultDiv.innerHTML = `${formData.num} is not a prime number.`;
            }
            else {
                resultDiv.innerHTML = `${response.Error}`;
            }
        } else {
            console.error('Error:', xhr.statusText);
        }
    };

    // Handle network errors
    xhr.onerror = function () {
        console.error('Network error');
    };

    // Send the POST request with the form data as JSON
    xhr.send(JSON.stringify(formData));
}
