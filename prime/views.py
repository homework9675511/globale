from rest_framework.response import Response
from rest_framework.decorators import api_view
import datetime
from django.core.cache import cache


@api_view(['POST'])
def IsPrimeNumberApi(request):
    """
    this is the main api endpoint, most of the project surround it
    the post request should be like - {id: some id, num: some number}
    """

    if request.method == 'POST':
        input = request.data
        id = input['id']
        num = input['num']
        limit = rate_limiter(id)
        output = {
                'HasError': check_input(input),
                'number': num,
                'IsPrime': IsPrimeNumber(num)
            }
        if limit != True:
            output = {
                'HasError': True,
                'Error': f'you need to wait {limit} seconds'
            }
        # rate_limiter(id)
        print(output)
        return Response(output)


def check_input(input):
    """
    check if the user's values are correct
    """
    id = input['id']
    num = str(input['num'])

    if len(id) > 100:
        return True
    if len(num) > 10:
        return True
    return False


def IsPrimeNumber(num):
    # Program to check if a number is prime or not

    # define a flag variable
    flag = False

    if num == 1:
        return False
    elif num > 1:
        # check for factors
        for i in range(2, num):
            if (num % i) == 0:
                # if factor is found, set flag to True
                flag = True
                # break out of loop
                break

        # check if flag is True
        if flag:
            return False
        else:
            return True


def rate_limiter(id):

    max_sec = 30
    currentTime = datetime.datetime.now()

    # Check if the 'ids' dictionary exists in the cache; if not, initialize it.
    if cache.get('ids') is None:
        cache.set('ids', {}, timeout=3600)
    ids = cache.get('ids')

    # Get the list of 'id's currently in the cache.
    list_of_ids = ids.keys()

    # If the given 'id' is not in the list of 'ids,' initialize it.
    if id not in list_of_ids:
        mini_id = [1, currentTime]  # mini_id is a list with count and timestamp.
        ids[id] = mini_id

    else:
        # If the 'id' exists, increment the request count.
        ids[id][0] += 1

    # If the request count for the 'id' reaches 2, check the time.
    if ids[id][0] == 2:
        seconds = (currentTime - ids[id][1]).total_seconds()

        # If the elapsed time is greater than the max_sec, reset the count and timestamp.
        if seconds > max_sec:
            ids[id][0] = 0
            ids[id][1] = currentTime

        # If not, reset the count and return the remaining time until the next request is allowed.
        else:
            ids[id][1] = currentTime
            ids[id][0] = 0
            return max_sec-seconds  # Indicates the time remaining until the next request is not allowed.

    # Update the 'ids' dictionary in the cache and return True, indicating the request is allowed.
    cache.set('ids', ids, timeout=3600)
    return True
